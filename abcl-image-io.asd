;;;; abcl-image-io.asd

(asdf:defsystem #:abcl-image-io
  :description "Simple utility for generate images from array of integers"
  :author "alejandrozf"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "abcl-image-io")))
