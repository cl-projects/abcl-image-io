# abcl-image-io
### _alejandrozf_

Simple utility for generate images from array of integers, using Java's ImageIO

Usage:

```
CL-USER> (ql:quickload :abcl-image-io)
To load "abcl-image-io":
  Load 1 ASDF system:
    abcl-image-io
; Loading "abcl-image-io"

(:ABCL-IMAGE-IO)
CL-USER> (abcl-image-io:create-image 30 30 "test1.png" #'identity)
T
CL-USER> (abcl-image-io:create-image-from-pixel-list
          30 30 "test2.png"
          (loop :for i :below (* 30 30 3) :collect #xffaa77))
T
CL-USER> (abcl-image-io:create-image
          30 30 "test3.png"
          (lambda (i)
            (random 1000)))
T
CL-USER>
```

## License

MIT
