;;;; package.lisp

(defpackage #:abcl-image-io
  (:use #:cl)
  (:export #:create-image
           #:create-image-from-pixel-list
           #:create-image-from-pixel-jarray
           #:read-image-to-array))
