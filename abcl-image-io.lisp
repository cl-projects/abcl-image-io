;;;; abcl-image-io.lisp


(in-package #:abcl-image-io)


(defmacro with-image-io-artifacts (h w filename format &body body)
  `(let* ((buff-image (java:jnew "java.awt.image.BufferedImage" ,h ,w (java:jfield "java.awt.image.BufferedImage" "TYPE_INT_RGB")))
          (raster (java:jcall (java:jmethod "java.awt.image.BufferedImage" "getRaster") buff-image))
          (pixels ,@body)
          (file (java:jnew "java.io.File" ,filename)))

     (java:jcall (java:jmethod "sun.awt.image.IntegerInterleavedRaster" "setPixels" "int" "int" "int" "int" "int[]") raster
                 0 0 ,h ,w pixels)

     (java:jstatic (java:jmethod "javax.imageio.ImageIO" "write" "java.awt.image.RenderedImage"
                                 "java.lang.String" "java.io.File")
                   "javax.imageio.ImageIO"
                   buff-image ,format file)))


(defun create-image (h w filename pixel-function &key (format "png"))
  "Create an image by applying PIXEL-FUNCTION to every index of the array of (* h w 3) length that will contain the pixels"
  (abcl-image-io::with-image-io-artifacts h w filename format
    (java:jnew-array-from-list "int" (loop :for i :below (* h w 3) :collect (funcall pixel-function i)))))


(defun create-image-from-pixel-list (h w filename pixels &key (format "png"))
  "Create an image by using a previous created list of RGB pixels  with length equal to (* h w 3)"
  (abcl-image-io::with-image-io-artifacts h w filename format (java:jnew-array-from-list "int" pixels)))


(defun create-image-from-pixel-jarray (h w filename pixels-jarray &key (format "png"))
  "Create an image by using a previous created jarray of RGB pixels with length equal to (* h w 3)"
  (abcl-image-io::with-image-io-artifacts h w filename format pixels-jarray))


(defun read-image-to-array (image-file)
  "Returns 4 values array with  arra object that contains all the information of the image"
  (let* ((file (java:jnew "java.io.File" image-file))
         (buff-image (java:jstatic (java:jmethod "javax.imageio.ImageIO" "read" "java.io.File")
                                   "javax.imageio.ImageIO" file))
         (raster (java:jcall (java:jmethod "java.awt.image.BufferedImage" "getRaster") buff-image))
         (raster-h (java:jcall "getHeight" raster))
         (raster-w (java:jcall "getWidth" raster))
         (pixels (java:jcall "getPixels" raster 0 0 raster-w raster-h
                             (java:jnew-array "int" (* raster-h raster-w 3)))))
    (values pixels raster-h raster-w raster)))
